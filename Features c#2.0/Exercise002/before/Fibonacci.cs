﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Exercise002.before
{
    public class Fibonacci
    {
        public static string Log { get; set; }

        public static IEnumerable<int> GenerateFibonacciNumbers(int n)
        {
            Log = string.Empty;

            //Log+=item
            return null;
        }
    }


    [TestFixture]
    public class FibonacciTest
    {
        [Test]
        public void Test1()
        {
            string expeted = null;

            List<int> listResult = new List<int>(Fibonacci.GenerateFibonacciNumbers(10));
            string real = Fibonacci.Log;


            foreach (var item in listResult)
            {
                expeted += item;
            }

            Assert.AreEqual(expeted, real, "Deben coincidir lo registrado por log final de GenerateFibonacciNumbers");
        }

        [Test]
        public void Test2()
        {
            string expeted = null;

            IEnumerable<int> generateFibonacciNumbers = Fibonacci.GenerateFibonacciNumbers(10);

            foreach (var item in generateFibonacciNumbers)
            {
                expeted += item;
                Assert.AreEqual(expeted, Fibonacci.Log, "Deben coincidir lo registrado por log de GenerateFibonacciNumbers en el momento evaludado");
            }

            Assert.AreEqual(expeted, Fibonacci.Log, "Deben coincidir lo registrado por log final de GenerateFibonacciNumbers");
        }

        [Test]
        public void Test3()
        {
            string expeted = "0112358";

            IEnumerable<int> generateFibonacciNumbers = Fibonacci.GenerateFibonacciNumbers(10);

            int index = 0;

            foreach (var item in generateFibonacciNumbers)
            {
                if (index++ > 5)
                    break;
            }

            Assert.AreEqual(expeted, Fibonacci.Log, "Deben coincidir lo registrado por log del generateFibonacciNumbers");
        }

    }
}