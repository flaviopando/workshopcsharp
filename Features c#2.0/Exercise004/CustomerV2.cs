﻿using System;

namespace Exercise004
{
    public partial class Customer
    {
      
        // Deposit function  
        public void DepositAmount(int d_amount)
        {
            amount += d_amount;
            Console.WriteLine(d_amount + " amount is deposited");
            Console.WriteLine("Available balance is: " + amount);
        }
    }
}