﻿using System;

namespace Exercise004
{
    public partial class Customer
    {
        private int amount;
        public int Amount { get => amount; set => amount = value; }
        // Withdraw function  
        public void Withdraw(int w_amount)
        {
            amount -= w_amount;
            Console.WriteLine(w_amount + " is withdrawn");
            Console.WriteLine("Available balance is: " + amount);
        }

      
    }
}