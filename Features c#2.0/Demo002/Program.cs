﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Demo002
{
    public class PowersOfTwo : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            for (int i = 1; i < 10; i++)
            {
                Console.Write("yield ");
                yield return (int)System.Math.Pow(2, i);
            }
        }
    }

    public class Program
    {
        static void Main()
        {
            PowersOfTwo powers = new PowersOfTwo();
            foreach (int pow in powers)
            {
                Console.Write(pow + " ");
            }
            // Output: yield 2 yield 4 yield 8 yield 16 yield 32 yield 64 yield 128 yield 256 yield 512

            Console.ReadKey();
        }
    }
}
