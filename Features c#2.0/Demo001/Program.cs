﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Demo001
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassName.A = 10;
            Console.WriteLine("Inicio");
            ClassName.Sum();
            ClassName.Sum();
            Console.WriteLine("Fin");
            Console.ReadKey();
           
        }
    }

    public static class ClassName
    {
        public static int A { get; set; }
        public static int B { get; set; }

        static ClassName()
        {
            Console.WriteLine("Constructor");
            A = 1;
            B = 2;
        }

        public static int Sum()
        {
            int sum = A + B;
            Console.WriteLine(sum);
            return sum;
        }
    }
}