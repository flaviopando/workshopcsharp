﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo004
{
    class Program
    {
        delegate void PrintDel(DateTime param);
        static void Main(string[] args)
        {
            PrintDel del =new PrintDel(PrintConsole);

            PrintDate(del);

            Console.ReadKey();
        }

        private static void PrintConsole(DateTime param)
        {
            Console.WriteLine(param);
        }

        private static void PrintDate(PrintDel del)
        {
            del(DateTime.Now);
        }
    }


    
}
