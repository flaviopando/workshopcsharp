﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Exercise003.before
{
    public class DaysOfTheWeek : IEnumerable
    {
        private string[] Days = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        public List<string> Log = new List<string>();


        public IEnumerator GetEnumerator()
        {
            Log = new List<string>();
            throw new NotImplementedException();
        }
    }

    [TestFixture]
    public class DaysOfTheWeekTest
    {
        [Test]
        public void Test1()
        {
            List<string> daysList = new List<string>();
            DaysOfTheWeek daysOfTheWeek = new DaysOfTheWeek();


            foreach (string day in daysOfTheWeek)
            {
                daysList.Add(day);
                Assert.AreEqual(daysList, daysOfTheWeek.Log);
            }
        }

        [Test]
        public void Test2()
        {
            List<string> daysList = new List<string>();
            DaysOfTheWeek daysOfTheWeek = new DaysOfTheWeek();
            List<string> list = daysOfTheWeek.Cast<string>().ToList();

            
            Assert.AreEqual(new List<string> {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"},
                daysOfTheWeek.Log);


            foreach (string day in list)
            {
                daysList.Add(day);
            }


            Assert.AreEqual(new List<string> { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" },
                daysList);
        }
    }
}