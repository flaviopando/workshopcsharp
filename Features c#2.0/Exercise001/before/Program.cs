﻿using System;
using NUnit.Framework;

namespace Exercise001.before
{
    public class Program
    {
        public static int[] Reverse(int[] arrayInt)
        {
            throw new NotImplementedException();
        }
        public static string[] Reverse(string[] arrayInt)
        {
            throw new NotImplementedException();
        }
        public static object[] Reverse(object[] arrayInt)
        {
            throw new NotImplementedException();
        }
    }

    [TestFixture]
    public class ProgramTest
    {
        [Test]
        public void Test1()
        {
            int[] arrayInt = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            int[] result = Program.Reverse(arrayInt);
            int[] expected = new int[] { 8, 7, 6, 5, 4, 3, 2, 1 };
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Test2()
        {
            string[] strings = new string[] { "uno", "dos", "tres" };
            string[] result2 = Program.Reverse(strings);
            string[] expected2 = new string[] { "tres", "dos", "uno" };

            Assert.AreEqual(expected2, result2);
        }

        [Test]
        public void Test3()
        {
            object[] objects = new object[] { "test", 1, true, 3434d };
            object[] result2 = Program.Reverse(objects);
            object[] expected2 = new object[] { 3434d, true, 1, "test" };

            Assert.AreEqual(expected2, result2);
        }
    }
}