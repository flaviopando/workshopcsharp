﻿using System;

namespace EjemploDelegados
{
    class Program
    {

        public delegate void WorkPerformedHandler(int hours, int type);

        static void Main(string[] args)
        {
            WorkPerformedHandler del1 = WorkPerformed1;
            WorkPerformedHandler del2 = new WorkPerformedHandler(WorkPerformed2);
            WorkPerformedHandler del3 = new WorkPerformedHandler(WorkPerformed3);

            del1 += del2;
            del1 += del3;

            del1(5, 1);
            Console.ReadKey();
        }

        static void WorkPerformed1(int hoursWorked, int typeWork)
        {
            Console.WriteLine("llamado metodo WorkPerformed1");
        }

        static void WorkPerformed2(int hoursWorked, int typeWork)
        {
            Console.WriteLine("llamado metodo WorkPerformed2");
        }
        static void WorkPerformed3(int hoursWorked, int typeWork)
        {
            Console.WriteLine("llamado metodo WorkPerformed3");
        }
    }
}

