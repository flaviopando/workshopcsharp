﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtributteExcercise
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class ImprimirAttribute : Attribute
    {
        public bool Print { get; set; }
        public ImprimirAttribute(bool print)
        {
            Print = print;
        }
    }
}
