﻿using System;

namespace AtributteExcercise
{
    //1. Crear un archivo de texto que concatene los valores de la propiedades tipo string de la clase persona y 
    //restringir el atribuo para que solo sea tipo propiedad
    class Program
    {
        static void Main(string[] args)
        {
            Procesar prop = new Procesar();


            prop.ImprimirDatos(new Persona
                {Apellido = "Gonzalez", Ciudad = "Mendoza", Edad = 18, Nombre = "Damian", Sexo = 'M'});

            Console.ReadKey();
        }
    }
}