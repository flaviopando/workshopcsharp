﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AtributteExcercise
{
    public class Procesar
    {
        public void ImprimirDatos(Persona persona)
        {
            PropertyInfo[] prop = persona.GetType().GetProperties();

            foreach (PropertyInfo item in prop)
            {
                object[] attrs = item.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    ImprimirAttribute authAttr = attr as ImprimirAttribute;
                    if (authAttr != null && authAttr.Print)
                    {
                        Console.WriteLine(item.Name+" "+ item.GetValue(persona));
                    }
                }
            }
        }
    }
}
