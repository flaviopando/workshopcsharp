﻿namespace AtributteExcercise
{
    public class Persona
    {
        [Imprimir(true)]
        public string Nombre { get; set; }
        [Imprimir(true)]
        public string Apellido { get; set; }
        [Imprimir(false)]
        public int Edad { get; set; }

        public char Sexo { get; set; }
        public string Ciudad { get; set; }
    }
}
