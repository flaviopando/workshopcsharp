﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioEventos
{
   public class EnterName
    {
        public event EventHandler<BannedUserEventArgs> ev_BannedUser;
        public void User()
        {
            Console.Write("Ingrese su Nombre : ");
            string user = Console.ReadLine();

            if ((user == "Nico" || user == "Flavio" || user == "Damian") && (ev_BannedUser != null))
            {
                ev_BannedUser(this, new BannedUserEventArgs(user));
            }
            else
            {
                Console.WriteLine("Bienvenido " + user);
            }
        }
    }
}
