﻿using System;

namespace EjercicioEventos
{
    public class BannedUserEventArgs : EventArgs
    {
        public BannedUserEventArgs(string s)
        {
            Name = s;
        }
        public string Name { get; set; }
    }
}
