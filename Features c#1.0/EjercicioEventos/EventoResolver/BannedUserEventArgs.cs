﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventoResolver
{
    public class BannedUserEventArgs : EventArgs
    {
        public BannedUserEventArgs(string s)
        {
            Name = s;
        }
        public string Name { get; set; }
    }
}
