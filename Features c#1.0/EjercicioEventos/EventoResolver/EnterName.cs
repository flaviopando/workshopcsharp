﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventoResolver
{
    public class EnterName
    {
        public event EventHandler<BannedUserEventArgs> ev_BannedUser;
        public void User()
        {
           //Resolver
        }
    }
}


//Qu: Escriba un programa para asistencia en línea.Las condiciones son las siguientes:
//1: El usuario proporciona su nombre como Entrada y luego la aplicación muestra el mensaje "Bienvenido a su nombre".
//2: Jack, Steven y Mathew están prohibidos para la organización. Entonces, cuando cualquier usuario
//ingresa a Jack, Steven y Mathew como nombre de usuario, la aplicación genera un evento y una alarma de incendio,
//y también envía un correo electrónico a la administración.