﻿using System;

namespace EventoResolver
{
    class Program
    {
        static void Main(string[] args)
        {
            EnterName ename = new EnterName();
            ename.ev_BannedUser += WarningAlarm;
            ename.User();
            Console.Read();
        }
        static void WarningAlarm(object sender, BannedUserEventArgs e)
        {
            Console.WriteLine("{0} Usuario Reportado. Enciado Email a Administracion.", e.Name);
            Console.WriteLine("Correo Enviado.");
            Console.WriteLine("Alarma iniciada");
            Console.WriteLine("Presione Ctrl + c para detener alarma");
            for (; ; )
            {
                Console.Beep();
                System.Threading.Thread.Sleep(100);
            }
        }
    }
}
