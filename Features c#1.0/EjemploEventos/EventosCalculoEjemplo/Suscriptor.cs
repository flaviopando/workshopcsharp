﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosCalculoEjemplo
{
   public class Suscriptor
    {
        public Calculo calculos;

        
        public Suscriptor(Calculo calculos)
        {
            this.calculos = calculos;
            calculos.ResultadoPar += Calculos_ResultadoPar;
            calculos.ResultadoPar -= Calculos_ResultadoPar;
        }

        private void Calculos_ResultadoPar()
        {
            Console.WriteLine("Notificado 2");
        }
    }
}
