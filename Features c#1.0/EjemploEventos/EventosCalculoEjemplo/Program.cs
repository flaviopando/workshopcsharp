﻿using System;

namespace EventosCalculoEjemplo
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculos = new Calculo();
            calculos.ResultadoPar += Calculos_ResultadoPar;
            Suscriptor sus = new Suscriptor(calculos);
            calculos.Calcular();
            Console.ReadKey();
        }

        private static void Calculos_ResultadoPar()
        {
            Console.WriteLine("Notificado");
        }
    }
}
