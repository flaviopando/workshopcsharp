﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosCalculoEjemplo
{
    public class Calculo
    {
        public delegate void Resultado();
        public event Resultado ResultadoPar;

        public void Calcular()
        {
            Resultado evento = ResultadoPar;
            if(evento != null)
            {
                evento();
            }
        }
    }
}
