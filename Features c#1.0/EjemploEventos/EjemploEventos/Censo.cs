﻿using System;

namespace EjemploEventos
{
    public class Censo
    {
        private readonly Ciudad _ciudad;
       public delegate void CiudadEventHandler (Persona persona);
      

        public Censo(Ciudad c)
        {
            _ciudad = c;
            _ciudad.HabitanteDadoDeAlta += new CiudadEventHandler(Ciudad_HabitanteDadoDeAlta);
        }

        private void Ciudad_HabitanteDadoDeAlta(Persona persona)
        {
            Console.WriteLine("{0} vive ahora en {1}", persona.Nombre, _ciudad.Nombre);
        }
    }

}
