﻿using static EjemploEventos.Censo;

namespace EjemploEventos
{
    public class Ciudad
    {
        public string Nombre { get; set; }

        public event CiudadEventHandler HabitanteDadoDeAlta;

        public Ciudad(string nombre)
        {
            this.Nombre = nombre;
        }

        public void OnHabitanteDadoDeAlta(Persona persona)
        {
            if (HabitanteDadoDeAlta != null)
            {
                HabitanteDadoDeAlta(persona);
            }
        }

    }
}
