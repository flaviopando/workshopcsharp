﻿using System;

namespace EjemploEventos
{
    public class Persona
    {
        public string Nombre { get; set; }
        public Ciudad Ciudad { get; set; }

        public Persona(string nombre, Ciudad ciudad)
        {
            this.Nombre = nombre;
            Ciudad = ciudad;
        }

        internal void Emigrar(Ciudad ciudad)
        {
            Console.WriteLine(Nombre + "se mudo a " + ciudad.Nombre);
            ciudad.OnHabitanteDadoDeAlta(this);
            Console.ReadKey();
        }
    }
}
