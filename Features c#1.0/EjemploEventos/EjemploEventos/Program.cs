﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploEventos
{
    class Program
    {
        static void Main(string[] args)
        {

            var mendoza = new Ciudad("Mendoza");
            var castefa = new Ciudad("SanJuan");
            var bandolero = new Persona("NicoPico", mendoza);
            Console.ReadLine();
            bandolero.Emigrar(castefa);


        }
    }
}
