﻿using System;

namespace EjemploStruct
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector vectorEjempplo;

            vectorEjempplo.x = 5;
            vectorEjempplo.y = 6;
            vectorEjempplo.z = 7;

            Persona persona1 = new Persona();
            persona1.Nombre = "NicoPico";

            ModificarPersona(persona1);
            ModificarVector(vectorEjempplo);

            Console.WriteLine("valor de x "+ vectorEjempplo.x);
            Console.WriteLine("valor de y " + vectorEjempplo.y);
            Console.WriteLine("valor de z " + vectorEjempplo.z);

            Console.WriteLine("Nombre " + persona1.Nombre);

            Console.ReadKey();

        }

        public static void ModificarVector(Vector vc)
        {
            vc.x = 1;
            vc.y = 1;
            vc.z = 1;
        }

        public static void ModificarPersona(Persona p)
        {
            p.Nombre = "CainCeleste";
        }
    }
}
