﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new List<Movie>
            {
                new Movie { Title = "La brigada explosiva.",   Rating = 8.9f, Year = 2008 },
                new Movie { Title = "Los bañeros mas locos del mundo.", Rating = 8.0f, Year = 2010 },
                new Movie { Title = "los caza monstruos en la mansion del terror.",        Rating = 8.5f, Year = 1942 },
                new Movie { Title = "Los pilotos mas locos del mundo",       Rating = 8.7f, Year = 1980 }
            };


            //var query = from movie in movies
            //    where movie.Year > 2000
            //    //orderby movie.Rating descending
            //    select movie;

            var enumerator = movies.GetEnumerator();

            while (enumerator.MoveNext())
            {
                Console.WriteLine($"Usando el enumerador: {enumerator.Current.Title} ");

            }

            Console.WriteLine();

            foreach (var item in movies)
            {
                Console.WriteLine($"Usando el foreach: {item.Title}");
            }

            enumerator.Dispose();
            Console.ReadKey();
            
        }
    }
}
