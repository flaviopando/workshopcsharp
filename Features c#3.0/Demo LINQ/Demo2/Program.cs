﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Example0();

            //Example1();

           // Example2();

            //Example3();
        }

        private static void Example0()
        {
            int result = Square(5);

           Console.WriteLine($"El cuadrado de 5 es {result}");

           Console.Read();
        }

        private static void Example1()
        {
            string[] cities = { "Mendoza", "San Luis", "Cordoba", "Bs As" };

            IEnumerable<string> result = Enumerable.Where(cities, delegate (string city) { return city.StartsWith("M"); });

            IEnumerable<string> result2 = cities.Where(delegate (string city) { return city.StartsWith("M"); });

            IEnumerable<string> result3 = cities.Where(city => city.StartsWith("M"));
        }

        private static void Example2()
        {
            var people = new List<Person>();

            var person1 = new Person
            {
                Age = 42,
                Name = "Flavio"
            };

            var person2 = new Person
            {
                Age = 35,
                Name = "Cristian"
            };

            var person3 = new Person
            {
                Age = 31,
                Name = "Nico"
            };
        
            people.Add(person1);
            people.Add(person2);
            people.Add(person3);

            IEnumerable<Person> result0 = Enumerable.Where(people, MatchName);

            IEnumerable<Person> result1 = Enumerable.Where(people, delegate(Person p) { return p.Name == "Flavio"; });

            IEnumerable<Person> result2 = people.Where(delegate(Person p) { return p.Name == "Flavio"; });

            IEnumerable<Person> result3 = people.Where(p => p.Name == "Flavio");
        }

        private static void Example3()
        {
            Func<Person,bool> matchName = (Person person) => person.Name == "Flavio";

            var people = new List<Person>();
            var person1 = new Person
            {
                Age = 42,
                Name = "Flavio"
            };

            var person2 = new Person
            {
                Age = 35,
                Name = "Cristian"
            };

            var person3 = new Person
            {
                Age = 31,
                Name = "Nico"
            };

            people.Add(person1);
            people.Add(person2);
            people.Add(person3);

            IEnumerable<Person> result0 = Enumerable.Where(people, matchName);

            IEnumerable<Person> result1 = people.Where(matchName);

        }

        public static bool MatchName(Person p)
        {
            return p.Name == "Flavio";
        }

        public static int Square(int number)
        {
            return number * number;
        }
    }
}
